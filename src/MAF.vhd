-- MAF.vhd
--
-- A simple moving average filter.
--
-- @author Rimon Oz
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MAF is
generic (
	-- The clock frequency (default 100MHz).
	clock_frequency: integer := 100000000;
	sample_rate:     integer := 100000000;
	-- The number of samples in history is equal to 2^(sample_size)
	sample_bits:     integer := 3
);
port (
	clock:  in  std_logic;
	reset:  in  std_logic;
	data_in:  in  std_logic_vector(7 downto 0);
	data_out: out std_logic_vector(7 downto 0) := "00000000"
);
end MAF;

architecture behavior of MAF is
	signal clock_signal:   std_logic;
begin
	eCLK:  entity work.MAFclock generic map(clock_frequency, sample_rate)
			            port    map(clock,           clock_signal);
	eMAF0: entity work.MAFcalc  generic map(sample_bits)
				    port    map(clock_signal,    reset,       data_in, data_out);
end behavior;
