-- MAFclock.vhd
--
-- A moving average filter sample clock. This entity makes the clock
-- take samples conform to a specific rate (samples per second), 
-- given the processor's clock frequency.
--
-- @author Rimon Oz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MAFclock is
generic (
	-- The clock frequency. default = 100Mhz
	clock_frequency: integer := 100000000;
	-- The sample rate (samples per sec).
	sample_rate:     integer := 10000000
);
port (
	clock_in:  in  std_logic;
	clock_out: out std_logic
);
end MAFclock;

architecture behavior of MAFclock is
	constant clocks_per_sample: integer := clock_frequency / sample_rate; 
	signal   counter:         integer   := 0;
begin
	process(clock_in)
	begin
		-- Trivial case.
		if clocks_per_sample = 1 then
			clock_out <= clock_in;
		else
			-- Otherwise count on every rising edge
			if rising_edge(clock_in) then
				-- until the threshold, and flip the sign.
				if counter = clocks_per_sample - 1 then
				  clock_out <= '1';
				  counter   <= 0;   
				else
				  clock_out <= '0';
				  counter   <= counter + 1;
				end if;
			end if;
		end if;
	end process;
end behavior;