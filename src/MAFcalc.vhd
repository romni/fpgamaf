-- MAFcalc.vhd
--
-- A moving average calculator.
--
-- @author Rimon Oz

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MAFcalc is
generic (
	-- The number of samples in history is 2^(sample_bits)
	sample_bits: integer := 3
);
port (
	clock:    in  std_logic;
	reset:    in  std_logic;
	data_in:  in  std_logic_vector(7 downto 0);
	data_out: out std_logic_vector(7 downto 0)
);
end MAFcalc;

architecture behavior of MAFcalc is
	constant HISTORY_SIZE: integer := 2**(sample_bits);
	
	-- SampleHistory is an array of vectors containing samples.
	type SampleHistory is array((HISTORY_SIZE - 1) downto 0) of std_logic_vector(7 downto 0);
begin
	process(clock, reset)
		variable counter:        integer                            := 0;
		variable sample_history: SampleHistory                      := (others => (others => '0'));
		-- sum_cache is used to calculate the sum of the samples.
	   variable sum_cache:      unsigned(8 + sample_bits downto 0) := (others => '0');
	begin
		if reset = '0' then
			counter        := 0;
			data_out       <= "00000000";
			sum_cache      := (others => '0');
			sample_history := (others => (others => '0'));
		else
			if rising_edge(clock) then
				-- Add the current number to the cache, and remove the oldest recorded value
				sum_cache := sum_cache + unsigned(data_in) - unsigned(sample_history(counter));
				-- Shift the data to the right by sample_bits (division by HISTORY_SIZE)
				-- and resize the unsigned value to fit the data_out register.
				data_out  <= std_logic_vector(resize(shift_right(sum_cache, sample_bits), 8));
				-- Make the data available
				sample_history(counter) := data_in;
				
				if counter = HISTORY_SIZE - 1 then
					counter := 0;
				else
					counter := counter + 1;
				end if;
			end if;
		end if;
	end process;
end behavior;