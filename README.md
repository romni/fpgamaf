# fpgamaf
> A simple digital moving average filter.

## How to build

Compile with your favorite VHDL compiler for your favorite platform!


## License

I hereby waive all copyright and any related rights to this work to the extent possible under law.
All of the source code in this repository is part of the public domain.
